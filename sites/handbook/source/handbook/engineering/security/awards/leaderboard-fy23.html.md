---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@vitallium](https://gitlab.com/vitallium) | 8 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 9 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 10 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 11 | 300 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 12 | 300 |
| [@ratchade](https://gitlab.com/ratchade) | 13 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 14 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 15 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 16 | 150 |
| [@10io](https://gitlab.com/10io) | 17 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 18 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 19 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 20 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 21 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 22 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 23 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 24 | 60 |
| [@minac](https://gitlab.com/minac) | 25 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 26 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 27 | 60 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 28 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 29 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 30 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 31 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 32 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 33 | 30 |
| [@subashis](https://gitlab.com/subashis) | 34 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 35 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 36 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 37 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 38 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 39 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 40 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@vitallium](https://gitlab.com/vitallium) | 8 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 9 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 10 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 11 | 300 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 12 | 300 |
| [@ratchade](https://gitlab.com/ratchade) | 13 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 14 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 15 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 16 | 150 |
| [@10io](https://gitlab.com/10io) | 17 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 18 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 19 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 20 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 21 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 22 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 23 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 24 | 60 |
| [@minac](https://gitlab.com/minac) | 25 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 26 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 27 | 60 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 28 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 29 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 30 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 31 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 32 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 33 | 30 |
| [@subashis](https://gitlab.com/subashis) | 34 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 35 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 36 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 37 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 38 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 39 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 40 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |


